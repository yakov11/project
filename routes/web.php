<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('discussions', 'DiscussionsController')->middleware('auth');
Route::get('mydiscussions', 'DiscussionsController@myDiscussions')->name('discussions.mydiscussions')->middleware('auth');
Route::get('discussions/delete/{id}', 'DiscussionsController@destroy')->name('discussion.delete');
Route::get('show', 'DiscussionsController@show')->name('discussion.show')->middleware('auth');
Route::get('pagin', 'DiscussionsController@pagin')->name('discussions.pagin')->middleware('auth');
Route::get('paginjudge', 'DiscussionsController@paginjudge')->name('discussions.paginjudge')->middleware('auth');

Route::get('paginhall', 'DiscussionsController@paginhall')->name('discussions.paginhall')->middleware('auth');
//Route::get('discussionsbydate', 'DiscussionsController@discussionsByDate')->name('discussions.discussionsbydate')->middleware('auth');

//rute for deleting bulk of discussion
Route::post('/del', 'DiscussionsController@del')->middleware('auth');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('halls', 'DiscussionsController@halls')->name('discussions.halls')->middleware('auth');
Route::get('judges', 'DiscussionsController@judges')->name('discussions.judges')->middleware('auth');
Route::get('writers', 'DiscussionsController@writers')->name('discussions.writers')->middleware('auth');


Route::get('judgediscussions/{id}', 'DiscussionsController@judgeDiscussions')->name('discussions.judgediscussions')->middleware('auth');
Route::get('writerdiscussions/{id}', 'DiscussionsController@writerDiscussions')->name('discussions.writerdiscussions')->middleware('auth');
Route::get('halldiscussions/{id}', 'DiscussionsController@hallDiscussions')->name('discussions.halldiscussions')->middleware('auth');
Route::get('searchDate', 'DiscussionsController@searchDate')->name('search.date')->middleware('auth');