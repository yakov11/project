<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            [
                'name' => 'Criminal',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Labor and Employment',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [
                'name' => 'Family',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Traffic',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Military',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Political',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],  
             ]);   
    }
}
