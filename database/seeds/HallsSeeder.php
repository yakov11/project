<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class HallsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('halls')->insert([
            [
                'name' => 'A',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'B',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [
                'name' => 'C',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'D',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'E',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
             ]);        
    }
}
