<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Discussion extends Model
{
    protected $fillable = ['date', 'hour', 'hall_id', 'judge_id', 'writer_id', 'type_id'];

    public function judge(){
        return $this->belongsTo('App\Judge');
    }
    public function writer(){
        return $this->belongsTo('App\Writer');
    }
    public function hall(){
        return $this->belongsTo('App\Hall');
    }
    public function type(){
        return $this->belongsTo('App\Type');
    }
    public static function hourallowed($hour, $date,$judge, $hall, $writer){
        $judgeall = DB::table('discussions')->where('hour',$hour)->where('date',$date)->where('judge_id',$judge)->get();
        var_dump($judge);
        if(count($judgeall)>0) {
          
           
        return false;}
        
            else{
                $hallall = DB::table('discussions')->where('hour',$hour)->where('date',$date)->where('hall_id',$hall)->get();
                if(count($hallall)>0) {
                    
                    return false;}
                    
                        else{   
                $writerall = DB::table('discussions')->where('hour',$hour)->where('date',$date)->where('writer_id',$writer)->get();
                if(count($writerall)>0) {
                    
                    return false;}
                    
                        else{                           
                            return true;}
     
        
    }
    
}}}  

