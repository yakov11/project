<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Judge extends Model
{

    public function user(){
        return $this->hasOne('App\User');
    }

    public function discussion(){
        return $this->hasMany('App\Discussion');
    }
    public function type(){
        return $this->belongsTo('App\Type');
        
    }
    public static function judgeType($type_id){
        
        $judgeAllowed = DB::table('judges')->where('type_id',$type_id)->pluck('name');
        var_dump($judgeAllowed);
        return self::find($judgeAllowed)->all();
        
    }

}
