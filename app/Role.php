<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Role extends Model
{
    public function users(){
        return $this->belongsToMany('App\User','userroles');        
     }
}
