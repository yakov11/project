<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Type extends Model
{
    public function discussion(){
        return $this->hasMany('App\Discussion');
    }
    public function judge(){
        return $this->hasMany('App\Judge');
    }
}
