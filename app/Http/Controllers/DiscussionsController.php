<?php

namespace App\Http\Controllers;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use App\Discussion;
use App\Hall;
use App\Judge;
use App\Type;
use App\Writer;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class DiscussionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $discussions = Discussion::orderBy('date','asc')->paginate(5);
        $halls = Hall::all();
        $judges = Judge::all();  
        $types = Type::all();
        $writers = Writer::all();
        $users = User::all(); 
             
        return view('discussions.index', compact('discussions','halls', 'judges','types','writers','users'));
    }
    public function pagin()
    {
        $discussions = Discussion::orderBy('date','asc')->paginate(5);
        $halls = Hall::all();
        $judges = Judge::all();  
        $types = Type::all();
        $writers = Writer::all();
        $users = User::all(); 
             
        return view('discussions.index', compact('discussions','halls', 'judges','types','writers','users'));
    }
    public function paginjudge()
    {
        $discussions = Discussion::orderBy('judge_id','asc')->paginate(5);
        $halls = Hall::all();
        $judges = Judge::all();  
        $types = Type::all();
        $writers = Writer::all();
        $users = User::all(); 
             
        return view('discussions.index', compact('discussions','halls', 'judges','types','writers','users'));
    }
    public function paginhall()
    {
        $discussions = Discussion::orderBy('hall_id','asc')->paginate(5);
        $halls = Hall::all();
        $judges = Judge::all();  
        $types = Type::all();
        $writers = Writer::all();
        $users = User::all(); 
             
        return view('discussions.index', compact('discussions','halls', 'judges','types','writers','users'));
    }
    

    public function myDiscussions()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $discussions = $user->discussions;
        //$candidates = Candidate::all();
        $users = User::all();
               
        return view('discussions.index', compact('discussions','users'));
    
    }


    public function halls()
    {
        $halls = Hall::all();
        return view('discussions.halls', compact('halls'));
    }

    public function judges()
    {
        $judges = Judge::all();
        return view('discussions.judges', compact('judges'));
    }

    public function writers()
    {
        $writers = Writer::all();
        return view('discussions.writers', compact('writers'));
    }


    public function judgeDiscussions($id)
    {     
        $judge = Judge::findOrFail($id);
        $discussions = $judge->discussion;
        $halls = Hall::all();
        $judges = Judge::all();  
        $types = Type::all();
        $writers = Writer::all();

        return view('discussions.judgediscussions', compact('judge', 'discussions', 'writers', 'halls', 'judges', 'types'));
    }

    public function writerDiscussions($id)
    {     
        $writer = Writer::findOrFail($id);
        $discussions = $writer->discussion;
        $halls = Hall::all();
        $judges = Judge::all();  
        $types = Type::all();
        $writers = Writer::all();

        return view('discussions.writerdiscussions', compact('writer', 'discussions', 'writers', 'halls', 'judges', 'types'));
    }

    public function hallDiscussions($id)
    {     
        $hall = Hall::findOrFail($id);
        $discussions = $hall->discussion;
        $halls = Hall::all();
        $judges = Judge::all();  
        $types = Type::all();
        $writers = Writer::all();

        return view('discussions.halldiscussions', compact('hall', 'discussions', 'writers', 'halls', 'judges', 'types'));
    }



    // public function discussionsByDate()
    // {        
    //     $discussions = Discussion::orderBy('date','asc')->paginate();
    //     $halls = Hall::all();
    //     $judges = Judge::all();  
    //     $types = Type::all();
    //     $writers = Writer::all();
    //     $users = User::all();       
    //     return view('discussions.index', compact('discussions','halls', 'judges','types','writers','users'));
    // }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('check-admin');
        $halls = Hall::all();
        $judges = Judge::all();
        $writers = Writer::all();
        $types = Type::all();
        return view('discussions.create', compact('halls','judges','writers','types'));
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $discussion = new Discussion();
       
        $hour=$request->hour;
        $date = $request->date;
        $judge =$request->judge_id;
        $hall =$request->hall_id;
        $writer = $request->writer_id;

        if (Discussion::hourallowed($hour, $date,$judge, $hall, $writer ) == true){       
        $dis = $discussion->create($request->all());
        $dis->save();
        return redirect('discussions'); 
    }
        else{
            return view('discussions.notallowed'); 
        }
        
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        if(Gate::allows('check-admin'))
        {   
        $discussion = Discussion::findOrFail($id);       
        $halls = Hall::all();
        $judges = Judge::all();
        $writers = Writer::all();
        $types = Type::all();
        return view('discussions.edit', compact('discussion','halls','judges','writers','types'));
        }else{
            Session::flash('no', 'sorry, only a Admin can make changes');
        } 
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $discussion = Discussion::findOrFail($id);

        $hour= $request->hour;
        $date= $request->date;
        $judge= $request->judge_id;
        $hall= $request->hall_id;
        $writer= $request->writer_id;
        
        
        if (Discussion::hourallowed($hour, $date,$judge, $hall, $writer ) == true){         
          $discussion->update($request->all());
          return redirect('discussions'); 
    }
        else{
            
             return view('discussions.notallowededit'); 
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $discussion = Discussion::findOrFail($id);
        $discussion->delete(); 
        return redirect('discussions'); 
    }

    public function del(Request $request)
    {
        Gate::authorize('check-admin');
        $delid = $request->input('delid');
        $toDelete = Discussion::whereIn('id', $delid);
        $toDelete->delete();
        return redirect('discussions'); 

    }

    public function searchDate(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        $discussions = Discussion::orderBy('date', 'asc')->whereBetween('date',[$from,$to])->paginate(5);   
        return view('discussions.index', compact('discussions'));
    }

}
