<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Discussion;
use App\Hall;
use App\Judge;
use App\Type;
use App\Writer;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $discussion = Discussion::all();
        $halls = Hall::all();
        $judges = Judge::all();
        $writers = Writer::all();
        return view('home', compact('discussion', 'halls', 'judges', 'writers'));
    }
}
