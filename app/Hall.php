<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Hall extends Model
{
    public function discussion(){
        return $this->hasMany('App\Discussion');
    }
}

