<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Writer extends Model
{
    public function discussion(){
        return $this->hasMany('App\Discussion');
    }

    public function user(){
        return $this->hasOne('App\User');
    }
}
