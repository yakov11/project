<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <div id="img-holder"> 
	
</div> 
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/816b35a905.js" crossorigin="anonymous"></script>


</head>
<body>
    <div id="app">
        @auth() 
        <main class="py-4 container-fluid">
            <div id="sidebar" class="sidebar float-left">
                <div class="text-center">
                <i class="fas fa-users fa-4x"></i>
                    <h2> {{ Auth::user()->name }} </h2>
                </div>
                <div class="items-container"></div>
                    <a href="{{ url('/home') }}">
                        <h5 class="menu-item"> <i class="fas fa-info"></i> Dashboard</h5>
                    </a>
                    <a href="{{ url('/discussions') }}">
                        <h5 class="menu-item"> <i class="fas fa-gavel"></i> All Discussion</h5>
                    </a>
                   
                    <a href="{{ route('discussions.paginjudge') }}">
                        <h5 class="menu-item"><i class="fas fa-clock"></i> Sort by judge name</h5>
                    </a>
                    <a href="{{ route('discussions.paginhall') }}">
                        <h5 class="menu-item"><i class="fas fa-clock"></i> Sort by hall </h5>
                    </a>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <h5 class="menu-item"><i class="fas fa-sign-out-alt"></i> {{ __('Logout') }} </h5>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                         @csrf
                    </form>
                    


                </div>
            </div>
        </main>
        @endauth
        <div class="content float_center">
                @yield('content')
        </div>
        <div class="content">
                @yield('content1')
        </div>

    </div>
</body>
</html>
