@extends('layouts.app')

@section('content')


<!-- Content Header (Page header) -->
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class ="m-0 text-dark"><b style=color:#20B2AA>Dashboard</b></h1>
            <br>
          </div><!-- /.col --> 
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div id="img-holder"> 
	
</div> 
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
          
            <!-- small box -->
            <div class="card bg-light xs-3" style = "width: 15rem;">
            <br>
            <i class="fas fa-comments fa-3x"style="text-align:center"></i>
            <br>
              <div class="inner" style="text-align:center">
                <h1 class="card-title">{{$discussion -> count()}}</h1>

                <h3><b>Discussions</b></h3>
                <br>

              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              
              <a href= "{{url('/discussions/create')}}" class="small-box-footer" style="text-align:center;color:#20B2AA"> New Discussion <i class="fas fa-arrow-circle-right" ></i></a>
             <br>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="card bg-light xs-3" style = "width: 15rem;">
            <br>
            <i class="fas fa-warehouse fa-3x"style="text-align:center"></i>
            <br>
              <div class="inner" style="text-align:center">
                <h1 class="card-title">{{$halls -> count()}}</h1>

                <h3><b>Halls</b></h3>
                <br>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="{{ route('discussions.halls') }}" class="small-box-footer"  style="text-align:center;color:#20B2AA">More info <i class="fas fa-arrow-circle-right"></i></a>
              <br>
           </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="card bg-light xs-3" style = "width: 15rem;">
            <br>
            <i class="fas fa-gavel fa-3x"style="text-align:center"></i>
            <br>
              <div class="inner" style="text-align:center">
          
                <h1 class="card-title">{{$judges -> count()}}</h1>

                <h3><b>Judges</b></h3>
                <br>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="{{ route('discussions.judges') }}" class="small-box-footer" style="text-align:center;color:#20B2AA">More info <i class="fas fa-arrow-circle-right" ></i></a>
              <br>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="card bg-light xs-3" style = "width: 15rem;">
            <br>
            <i class="fas fa-keyboard fa-3x"style="text-align:center"></i>
            <br>
              <div class="inner" style="text-align:center">
              <h1 class="card-title">{{$writers -> count()}}</h1>

                <h3><b>Writers</b></h3>
                <br>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="{{ route('discussions.writers') }}" class="small-box-footer"  style="text-align:center;color:#20B2AA">More info <i class="fas fa-arrow-circle-right" ></i></a>
              <br>
            </div>
          </div>
          <!-- ./col -->
        </div>
       

@endsection
