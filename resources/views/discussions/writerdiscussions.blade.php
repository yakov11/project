@extends('layouts.app')
@section('title', 'Discussions')
@section('content')

<h3 class ="m-0 text-dark"><b style=color:#20B2AA>{{ $writer->name}}{{ __(' Discussion') }}</b></h3>
<br>
                            

@csrf


<table class = "table table-bordered">
    <tr>
    <th>id</th><th>Date</th><th>Hour</th><th>Hall name</th><th>Judge name</th><th>discussion type</th>
    </tr>
    
    @foreach($discussions as $discussion)
        <tr>
           
            <td>{{$discussion->id}}</td>
            <td>{{$discussion->date}}</td>
            <td>{{$discussion->hour}}</td>
            <td>{{$discussion->hall->name}}</td>
            <td>{{$discussion->judge->name}}</td>
            <td>{{$discussion->type->name}}</td>
            
                </form>
            </td> 
                                                                      
        </tr>
    @endforeach
</table>
</form>


@endsection