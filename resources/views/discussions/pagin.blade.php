@extends('layouts.app')
@section('title', 'Discussions')
@section('content')


<h1>List of discussions</h1>
<table class ="table table-striped">
    <tr>
        <th>id</th><th>Date</th><th>Hour</th><th>Hall name</th><th>Judge name</th><th>Writer name</th><th>discussion type</th><th></th><th></th>
    </tr>
    
    @foreach($discussions as $discussion)
        <tr>
            <td>{{$discussion->id}}</td>
            <td>{{$discussion->date}}</td>
            <td>{{$discussion->hour}}</td>
            <td>{{$discussion->hall->name}}</td>
            <td>{{$discussion->judge->name}}</td>
            <td>{{$discussion->writer->name}}</td>
            <td>{{$discussion->type->name}}</td>
            <td>    
                <a class="dropdown-item" href="{{ route('discussions.edit', $discussion->id) }}">{{ __('Edit') }}</a>
            </td>  
            <td>
                <form action="{{ route('discussion.delete', $discussion->id) }}" method="get">
                <button type="button" class="dropdown-item" onclick="confirm('{{ __("Are you sure you want to delete this meeting?") }}') ? this.parentElement.submit() : ''">
                    {{ __('Delete') }}
                </button>
                </form>
            </td> 
                                                                      
        </tr>
    @endforeach
</table>
<nav aria-label="Page navigation example">
  <ul class="pagination">
    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">Next</a></li>
  </ul>
</nav>
<div><a href =  "{{url('/discussions/create')}}" class="btn btn-info btn-block">Add new discussion</a></div>

@endsection
