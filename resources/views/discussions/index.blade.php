@extends('layouts.app')
@section('title', 'Discussions')
@section('content')
@section('content')
@if(Session::has('no'))
<div class = 'alert alert-danger'>
    {{Session::get('no')}}
</div>
@endif
<div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                        <div class="row align-items-center">
                            <div class="col-10">
                            <h1 class ="m-0 text-dark"><b style=color:#20B2AA>Discussions</b></h1>
                            <br>

                            <a href = "{{url('/discussions/create')}}"  class="btn btn-outline-light btn-s" style="background-color: #20B2AA">Add new discussion</a>


                            <div class="card-body">
                        <form method="GET" action="{{route('search.date')}}">
                            <div class="row">
                                   From
                                <div class="col-md-3">
                                    <input type = "date" class="form-control" name = "from" placeholder="From Date">
                                </div>
                                <div class="input-group-addon">
                                    To
                                </div>
                                <div class="col-md-3">
                                    <input type = "date" class="form-control" name = "to" placeholder="To Date">
                                </div>
                                <div class="col-md-3">
                                <button type="submit" class="btn btn-outline-light btn-s" style="background-color: #20B2AA"><i class="fa fa-search"></i></button>
                                    <a href = "{{url('/discussions/')}}" class="btn btn-outline-light btn-s" style="background-color: #20B2AA">All Discussions</a>
                                </div> 
                                
                        </form>  



    <form action="{{ url('/del') }}" method="post">
    @csrf

    <br> 

<table class = "table table-bordered">
    <tr>
    <th>#</th><th>id</th><th>Date</th><th>Hour</th><th>Hall name</th><th>Judge name</th><th>Writer name</th><th>discussion type</th><th></th>
    
    @foreach($discussions as $discussion)
        @if($discussion->date <= Carbon::now())               
        <tr style = "background-color:#FFFFCC">
        @else    
        <tr>
        @endif
        
            <td><input type="checkbox" name="delid[]" value="{{$discussion->id}}"></td>
            <td>{{$discussion->id}}</td>
            <td>{{$discussion->date}}</td>
            <td>{{$discussion->hour}}</td>
            <td><a href = "{{route('discussions.halldiscussions', $discussion->hall->id)}}">{{$discussion->hall->name}}</td>
            <td><a href = "{{route('discussions.judgediscussions', $discussion->judge->id)}}">{{$discussion->judge->name}}</td>
            <td><a href = "{{route('discussions.writerdiscussions', $discussion->writer->id)}}">{{$discussion->writer->name}}</td>
            <td>{{$discussion->type->name}}</td>
            
           
            @if($discussion->date >= Carbon::now())
            <td>
            <a class="dropdown-item" href="{{ route('discussions.edit', $discussion->id) }}">{{ __('Edit') }}</a>
            </td>
            @else    
        <td></td>
        @endif                                                       
        </tr>
    @endforeach
</table>

<button type="submit" class="btn btn-outline-light btn-s" style="background-color: #F08080">Delete Selected discussion</button>

<p>{{$discussions->links()}}</p>
</form>

</div> 



@endsection
