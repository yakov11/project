@extends('layouts.app')

@section('title', 'Edit discussion')

@section('content')  
<div class="col-md-5">
<a href =  "{{url('/discussions')}}" class="btn btn-outline-light btn-lg"  style="background-color: #20B2AA;">go back to all discussions</a>

<h1 class ="m-0 text-dark"><b style=color:#20B2AA>Edit discussion</b></h1>
</div>


        <form method = "post" action = "{{action('DiscussionsController@update',$discussion->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="col-md-5">
            <label for = "date"><b>choose date</b></label>
            <input type = "date" class="form-control" name = "date" value= {{ $discussion->date }} >
        </div> 
        <br>
        <div class="col-md-5">
        <label for = "hour"><b>choose hour</b></label>
         
                        <select class="form-control" name="hour" >                                                                         
                        <option value="8-10">8-10</option>
                        <option value="10-12">10-12</option>
                        <option value="12-14">12-14</option>
                        <option value="14-16">14-16</option>
  </select>
                        </div>

        <br> 
        <div class="col-md-5">
        <label for = "hall_id"><b>choose hall</b></label>
                        <select class="form-control" name="hall_id">   
                        <option value="" disabled selected>{{$discussion->hall->name }}</option>                                                                      
                          @foreach ($halls as $hall)
                          <option value="{{$hall->id }}">    
                              {{$hall->name}} 
                          </option>
                          @endforeach    
                        </select>
                        </div>
                        <br>



                        <div class="col-md-5">
        <label for = "user_id"><b>choose judge</b></label>
                        <select class="form-control" name="judge_id" > 
                        <option value="" disabled selected>{{$discussion->judge->name }}</option>                                                                      
                          @foreach ($judges as $judge)
                          <option value="{{$judge->id }}">
                              {{$judge->name}} 
                          </option>
                          @endforeach    
                        </select>
                        </div>
                        
                        <br>
                        <div class="col-md-5">
        <label for = "hall_id"><b>choose writer</b></label>
                        <select class="form-control" name="writer_id">                                                                         
                        <option value="" disabled selected>{{$discussion->writer->name }}</option>                                                                      
                          @foreach ($writers as $writer)
                          <option value="{{$writer->id }}">
                              {{$writer->name}} 
                          </option>
                          @endforeach    
                        </select>
                        </div>
                        
                        <br>
                        <div class="col-md-5">
        <label for = "hall_id"><b>choose type</b></label>
                        <select class="form-control" name="type_id">                                                                         
                        <option value="" disabled selected>{{$discussion->type->name }}</option>                                                                      
                          @foreach ($types as $type)
                          <option value="{{$type->id }}">
                              {{$type->name}} 
                          </option>
                          @endforeach    
                        </select>
                        </div>
                       
                        <br>
             <div class="col-md-6">
            <input type = "submit" name = "submit" class="btn btn-outline-light btn-lg"  style="background-color: #20B2AA" value = "update discussion">
            </div> 
            <br>
        </div>            
        </form>    
@endsection